package edu.wgu.d387_sample_code.I18N;

        import org.springframework.http.HttpStatus;
        import org.springframework.http.ResponseEntity;
        import org.springframework.web.bind.annotation.CrossOrigin;
        import org.springframework.web.bind.annotation.GetMapping;
        import org.springframework.web.bind.annotation.RestController;
        
        @RestController
        @CrossOrigin(origins = "http://localhost:4200")
        public class TimeZoneConverterController {
        
            @GetMapping("/presentation")
            public ResponseEntity<String> announcePresentation() {
                java.lang.String livepresentation = "This is a reminder that there will be a live presentation at: " + TimeZoneConverter.getTime();
                //return new ResponseEntity<MysqlxDatatypes.Scalar.String> (livepresentation, HttpStatus.OK);
                return new ResponseEntity<String> (livepresentation, HttpStatus.OK);
            }
        }
