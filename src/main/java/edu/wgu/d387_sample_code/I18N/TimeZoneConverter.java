package edu.wgu.d387_sample_code.I18N;
    
    import java.time.*;
    import java.time.format.DateTimeFormatter;
    import org.springframework.web.bind.annotation.CrossOrigin;
    
    @CrossOrigin(origins = "http://localhost:4200")
    public class TimeZoneConverter {
    
        public static String getTime() {
        
            // getting to display time for live announcement
            ZonedDateTime time = ZonedDateTime.now();
            DateTimeFormatter timeFormat = DateTimeFormatter.ofPattern("HH:mm");
    
            ZonedDateTime est = time.withZoneSameInstant(ZoneId.of("America/New_York")); // Eastern
            ZonedDateTime mst = time.withZoneSameInstant(ZoneId.of("America/Denver")); // Mountain
            ZonedDateTime utc = time.withZoneSameInstant(ZoneId.of("UTC"));
            String times = est.format(timeFormat) + "EST, " + mst.format(timeFormat) + "MST, " + utc.format(timeFormat) + "UTC";
    
            return times;
        }
    }
