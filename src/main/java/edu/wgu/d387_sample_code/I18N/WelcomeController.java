package edu.wgu.d387_sample_code.I18L;
            
    import edu.wgu.d387_sample_code.I18N.welcomeMsg;
    import org.springframework.web.bind.annotation.CrossOrigin;
    import org.springframework.web.bind.annotation.GetMapping;
    import org.springframework.web.bind.annotation.RequestParam;
    import org.springframework.web.bind.annotation.RestController;
    import org.springframework.http.HttpStatus;
    import org.springframework.http.ResponseEntity;
    
    import java.util.Locale;
    
    @CrossOrigin(origins = "http://localhost:4200") 
    @RestController
    public class WelcomeController {
    
        @GetMapping("/welcome")
    
        public ResponseEntity<String> displayWelcome (@RequestParam("lang") String lang) { 
        
            // creating welcome message with multithread for en-fr
            Locale locale = Locale.forLanguageTag(lang); 
            // creatlocalobject
            welcomeMsg welcomeMsg = new welcomeMsg(locale);
            // language
            return new ResponseEntity<String> (welcomeMsg.getwelcomeMsg(), HttpStatus.OK);
            // return
        }
    }
