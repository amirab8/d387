package edu.wgu.d387_sample_code.I18N;
            
    import java.util.Locale;
    import java.util.ResourceBundle;
    
    public class welcomeMsg implements Runnable {
    
        Locale locale;
    
        //constructr
        public welcomeMsg(Locale locale) {
            this.locale = locale;
        }
    
        public String getwelcomeMsg() {
            ResourceBundle bundle = ResourceBundle.getBundle("translation",locale);
            return bundle.getString("welcome");
        }
    
        @Override
        public void run() {
            System.out.println( "Thread verification: " + getwelcomeMsg() + ", ThreadID: " + Thread.currentThread().getId() );
        }
    }

