package edu.wgu.d387_sample_code;
import edu.wgu.d387_sample_code.I18N.TimeZoneConverter;
import edu.wgu.d387_sample_code.I18N.TimeZoneConverterController;
import edu.wgu.d387_sample_code.I18N.welcomeMsg;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import java.util.Locale;
import java.io.InputStream;
import java.util.Properties;

@SpringBootApplication
public class D387SampleCodeApplication {

	public static void main(String[] args) {
		SpringApplication.run(D387SampleCodeApplication.class, args);
		
            // multithreads for welcomeMSGs'
            
            welcomeMsg welcomeMsgEnglish = new welcomeMsg(Locale.US);
            Thread englishWelcomeThread = new Thread(welcomeMsgEnglish);
            englishWelcomeThread.start();
            
            // welcomeMsg welcomeMsgSpanish = new welcomeMsg(Locale.MEXICO_SPANISH);
            // Thread spanishWelcomeThread = new Thread(welcomeMsgSpanish);
            // spanishWelcomeThread.start();
            
            welcomeMsg welcomeMsgFrench = new welcomeMsg(Locale.CANADA_FRENCH);
            Thread frenchWelcomeThread = new Thread(welcomeMsgFrench);
            frenchWelcomeThread.start();
           
		
		System.out.println("Time Conversion= " + TimeZoneConverter.getTime());
	}

}
