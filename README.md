BYN2 — BYN2 TASK 1: DEPLOYING A MODIFIED MULTITHREADED SPRING APPLICATION TO THE CLOUD
ADVANCED JAVA — D387
PRFA — BYN2
TASK OVERVIEW
SUBMISSIONS
EVALUATION REPORT
COMPETENCIES
4104.1.1 : Writes Mul�threaded Code
The learner writes mul�threaded, object-oriented code using Java frameworks.
4104.1.2 : Deploying Applica�ons with Cloud Services
The learner determines how to deploy so�ware applica�ons using cloud services.
INTRODUCTION
Throughout your so�ware design and development career, you will be asked to create, customize,
and maintain applica�ons with various features and func�onality based on business requirements.
For this assessment, you will modify a Spring applica�on with a Java back end and an Angular front
end to include mul�threaded language transla�on, a message at different �me zones, and currency
exchange. Then, you will build a Docker image of the current mul�threaded Spring applica�on and
containerize it using the suppor�ng document and web links provided.
The skills you showcase in your completed applica�on will be helpful in responding to technical
interview ques�ons for future employment. This applica�on may also be added to your por�olio to
show to prospec�ve employers.
Your submission should include a link to the project that contains the Dockerfile, a copy of the
repository branch history file, and the URL to the GitLab repository for evalua�on. The submission
must keep the project file and folder structure intact for the integrated development environment
(IDE).
Note: The IDE for this assessment is IntelliJ IDEA (Ul�mate Edi�on). Instruc�ons on how to
download this is provided in the a�ached suppor�ng document.
SCENARIO
You are working for a company located in Toronto, Canada, that schedules hotel reserva�ons. As a
so�ware developer, your job is to modify the Landon Hotel scheduling applica�on to meet new
requirements under new management. You will choose any applica�on user you would like.
You have been provided a Spring applica�on with a Java back end, an Angular front end, and web
links and a suppor�ng document to assist you in your work.
11/27/23, 14:55WGU Performance Assessment
2 of 6
https://tasks.wgu.edu/student/011396423/course/316...
REQUIREMENTS
Your submission must be your own original work. You may not use or reference other students'
submissions for this task. For more informa�on, please review our Academic Authen�city policies and
the College of Informa�on Technology Professionalism and Conduct Expecta�ons.
You must use the rubric to direct the crea�on of your submission because it provides detailed criteria
that will be used to evaluate your work. Each requirement below may be evaluated by more than one
rubric aspect. The rubric aspect �tles may contain hyperlinks to relevant por�ons of the course.
Tasks may not be submi�ed as cloud links, such as links to Google Docs, Google Slides, OneDrive,
etc., unless specified in the task requirements. All other submissions must be file types that are
uploaded and submi�ed as a�achments (e.g., .docx, .pdf, .ppt).
Note: External plug-ins and libraries other than those specified in this assessment are not allowed.
A. Create your subgroup and project in GitLab using the provided web link and the "GitLab How-To"
web link by doing the following:
• Clone the project to the IDE.
• Commit with a message and push when you complete each requirement listed in parts B1, B2,
B3, and C1.
Note: You may commit and push whenever you want to back up your changes, even if a
requirement is not yet complete.
• Submit a copy of the GitLab repository URL in the "Comments to Evaluator" sec�on when you
submit this assessment.
• Submit a copy of the repository branch history retrieved from your repository, which must
include the commit messages and dates.
Note: Wait un�l you have completed all the following prompts before you create your copy of
the repository branch history.
B. Modify the Landon Hotel scheduling applica�on for localiza�on and interna�onaliza�on by doing
the following:
1. Install the Landon Hotel scheduling applica�on in your integrated development environment
(IDE). Modify the Java classes of applica�on to display a welcome message by doing the
following:
a. Build resource bundles for both English and French (languages required by Canadian law).
Include a welcome message in the language resource bundles.
b. Display the welcome message in both English and French by applying the resource
bundles using a different thread for each language.
Note: You may use Google Translate for the wording of your welcome message.
2. Modify the front end to display the price for a reserva�on in currency rates for U.S. dollars ($),
Canadian dollars (C$), and euros (€) on different lines.
Note: It is not necessary to convert the values of the prices.
3. Display the �me for an online live presenta�on held at the Landon Hotel by doing the
following:
11/27/23, 14:55WGU Performance Assessment
3 of 6
https://tasks.wgu.edu/student/011396423/course/316...
a. Write a Java method to convert �mes between eastern �me (ET), mountain �me (MT), and
coordinated universal �me (UTC) zones.
b. Use the �me zone conversion method from part B3a to display a message sta�ng the �me
in all three �mes zones in hours and minutes for an online, live presenta�on held at the
Landon Hotel. The �mes should be displayed as ET, MT, and UTC.
C. Explain how you would deploy the Spring applica�on with a Java back end and an Angular front
end to cloud services and create a Dockerfile using the a�ached suppor�ng document "How to
Create a Docker Account" by doing the following:
1. Build the Dockerfile to create a single image that includes all code, including modifica�ons
made in parts B1 to B3. Commit and push the final Dockerfile to GitLab.
2. Test the Dockerfile by doing the following:
• Create a Docker image of the current mul�threaded Spring applica�on.
• Run the Docker image in a container and give the container a name that includes
D387_[student ID].
• Submit a screenshot capture of the running applica�on with evidence it is running in the
container.
3. Describe how you would deploy the current mul�threaded Spring applica�on to the cloud.
Include the name of the cloud service provider you would use.
Note: Remember to commit and push your changes to GitLab.
D. Demonstrate professional communica�on in the content and presenta�on of your submission.
File Restric�ons
File name may contain only le�ers, numbers, spaces, and these symbols: ! - _ . * ' ( )
File size limit: 400 MB
File types allowed: doc, docx, r�, xls, xlsx, ppt, pptx, odt, pdf, txt, qt, mov, mpg, avi, mp3, wav, mp4, wma,
flv, asf, mpeg, wmv, m4v, svg, �f, �ff, jpeg, jpg, gif, png, zip, rar, tar, 7z
RUBRIC
A:GITLAB REPOSITORY
NOT EVIDENT
COMPETENT
A GitLab repository is not APPROACHING
COMPETENCE provided. The subgroup and project are created in GitLab correctly,
created in GitLab, but 1 or and all of the given ac�ons are
more of the given ac�ons are completed correctly.
The subgroup and project are
not completed, or they are
completed incorrectly.
B1A:LANGUAGE RESOURCE BUNDLES
NOT EVIDENT
COMPETENT
The Landon Hotel scheduling APPROACHING
COMPETENCE applica�on does not include The Landon Hotel scheduling applica�on includes resource
language resource bundles, or applica�on includes language bundles for both English and
The Landon Hotel scheduling
11/27/23, 14:55WGU Performance Assessment
4 of 6
no code is provided.
https://tasks.wgu.edu/student/011396423/course/316...
resource bundles, but the French, including a welcome
bundles are not for both message built into the lan-
English and French. A wel- guage resource bundles. The
come message is not built code is complete and func-
into the language resource �ons correctly.
bundles. Or the code contains
errors or is incomplete.
B1B:DISPLAY WELCOME MESSAGE
NOT EVIDENT
COMPETENT
The Landon Hotel scheduling APPROACHING
COMPETENCE applica�on does not display a The Landon Hotel scheduling applica�on has the func�onal-
welcome message, or no code applica�on has limited func- ity to display a welcome mes-
is provided. �onality to display a welcome sage in both English and
message, or the language re- French by applying the re-
source bundles do not use a source bundles in the Java
different thread to display the classes using a different thread
languages in both English and for each language. The code is
French. Or the applica�on complete and func�ons cor-
does not use the Java classes rectly.
The Landon Hotel scheduling
resource bundles to display
the languages. Or the code
contains errors or is incom-
plete.
B2:DISPLAY CURRENCY RATES
NOT EVIDENT
COMPETENT
The Landon Hotel scheduling APPROACHING
COMPETENCE applica�on does not display a The Landon Hotel scheduling applica�on has the func�onal-
price for a reserva�on, or no applica�on displays the price ity to display the price for a
code is provided. for a reserva�on, but 1 or reserva�on in currency rates
more of the required currency for U.S. dollars ($), Canadian
rates are missing or are not on dollars (C$), and euros (€) on
different lines. Or the code different lines. The code is
contains errors or is incom- complete and func�ons cor-
plete. rectly.
COMPETENT
A �me zone conversion APPROACHING
COMPETENCE method is not provided. The Java method incorrectly converts the �me between
converts the �me between eastern �me, mountain �me,
eastern �me, mountain �me, and coordinated universal �me
The Landon Hotel scheduling
B3A:TIME ZONE CONVERSION METHOD
NOT EVIDENT
The Java method correctly
11/27/23, 14:55WGU Performance Assessment
5 of 6
https://tasks.wgu.edu/student/011396423/course/316...
and coordinated universal zones. The code is complete
�me zones. Or 1 or more of and func�ons correctly.
the required �me zones is
missing. Or the code contains
errors or is incomplete.
B3B:DISPLAY TIME ZONES
NOT EVIDENT
COMPETENT
The Landon Hotel scheduling APPROACHING
COMPETENCE applica�on does not display The Landon Hotel scheduling applica�on displays the �me
the �me for an online, live applica�on displays the �me of an online, live presenta�on
presenta�on held at the hotel, of an online, live presenta�on held at the hotel in all 3 �me
or no code is provided. held at the hotel, but 1 or zones from part B3a in hours
more of the required �me and minutes. The �mes are
zones from part B3a are miss- correctly displayed as ET, MT,
ing. Or the �me zones are not and UTC. The code is com-
correctly displayed as ET, MT, plete and func�ons correctly.
The Landon Hotel scheduling
and UTC. Or the code con-
tains errors or is incomplete.
C1:BUILD DOCKERFILE
NOT EVIDENT
A Dockerfile is not present.
APPROACHING
COMPETENCE COMPETENT
The Dockerfile image is cessfully built from the
present but does not success- Dockerfile and includes all
fully build from the code and modifica�ons made
Dockerfile, or it does not in- in parts B1 to B3.
The Dockerfile image is suc-
clude all code and modifica-
�ons made in parts B1 to B3.
C2:TEST THE DOCKERFILE
NOT EVIDENT
A Dockerfile is not tested.
APPROACHING
COMPETENCE COMPETENT
The Dockerfile is tested but includes all given requirements
does not include all the given without errors.
The Dockerfile is tested and
requirements, or some of the
listed requirements contain
errors.
C3:DESCRIPTION OF DEPLOYMENT TO THE CLOUD
NOT EVIDENT
COMPETENT
The descrip�on does not ex- APPROACHING
COMPETENCE plain how the current mul�- The descrip�on explains how plausible explana�on of how
The descrip�on provides a
11/27/23, 14:55WGU Performance Assessment
6 of 6
https://tasks.wgu.edu/student/011396423/course/316...
threaded Spring applica�on the current mul�threaded the current mul�threaded
could be deployed. Or the de- Spring applica�on could be Spring applica�on could be de-
scrip�on of deployment to deployed but does not plausi- ployed to the cloud that aligns
the cloud is not provided. bly align with the given cloud with the given cloud provider
provider services. Or a cloud services.
service provider is not in-
cluded.
D:PROFESSIONAL COMMUNICATION
NOT EVIDENT
COMPETENT
Content is unstructured, is APPROACHING
COMPETENCE disjointed, or contains perva- Content is poorly organized, detail, is organized, and fo-
sive errors in mechanics, us- is difficult to follow, or con- cuses on the main ideas as
age, or grammar. Vocabulary tains errors in mechanics, us- prescribed in the task or cho-
or tone is unprofessional or age, or grammar that cause sen by the candidate.
distracts from the topic. confusion. Terminology is mis- Terminology is per�nent, is
used or ineffec�ve. used correctly, and effec�vely
Content reflects a�en�on to
conveys the intended mean-
ing. Mechanics, usage, and
grammar promote accurate in-
terpreta�on and understand-
ing.
WEB LINKS
Docker
GitLab
Access Landon Hotel Scheduling Applica�on—Integrated Spring Back End and Angular Front End for
IntelliJ IDEA in this environment
GitLab How-To
IntelliJ IDEA (Ul�mate Edi�on) IDE – Student License
Node.js
How to Create a Docker Account
SUPPORTING DOCUMENTS
IntelliJ Ul�mate Edi�on Direc�ons.docx
11/27/23, 14:55
