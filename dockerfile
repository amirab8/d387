FROM openjdk:21-jdk-slim
COPY ./D387_sample_code.jar app.jar
WORKDIR ~/D287/d387/out
EXPOSE 8080
ENTRYPOINT ["java","-jar","/app.jar"]
CMD ["/home/user/D287/d387/D387_sample_code.jar"]
#ENTRYPOINT ["java", "-jar", "~/D287/d387/D387_sample_code_target.jar"]